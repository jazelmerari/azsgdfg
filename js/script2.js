const countryRange = document.getElementById("countryRange");
const selectedCountry = document.getElementById("selectedCountry");
const selectedCurrency = document.getElementById("selectedCurrency");
const currencyValue = document.getElementById("currencyValue");

const countryData = [
    {
        "country": "Estados Unidos",
        "currency": "Dólar",
        "value": 1
    },
    {
        "country": "México",
        "currency": "Peso Mexicano",
        "value": 18.15
    },
    {
        "country": "Francia",
        "currency": "Euro",
        "value": 0.95
    },
    {
        "country": "Canadá",
        "currency": "Dólar Canadiense",
        "value": 1.38
    },
    {
        "country": "Corea del Sur",
        "currency": "Won",
        "value": 1349.31
    },
    {
        "country": "China",
        "currency": "Yuan",
        "value": 7.32
    },
    {
        "country": "Japón",
        "currency": "Yen",
        "value": 150.24
    },
    {
        "country": "Argentina",
        "currency": "Peso Argentino",
        "value": 349.81
    },
    {
        "country": "Rusia",
        "currency": "Rublo Ruso",
        "value": 93.65
    },
    {
        "country": "Reino Unido",
        "currency": "Libra Esterlina",
        "value": 0.82
    },
    {
        "country": "India",
        "currency": "Rupia India",
        "value": 83.23
    },
    {
        "country": "Tailandia",
        "currency": "Baht tailandés",
        "value": 36.18
    },
    {
        "country": "Egipto",
        "currency": "Libra Egipcia",
        "value": 30.89
    },
    {
        "country": "Filipinas",
        "currency": "Peso Filipino",
        "value": 56.87
    },
    {
        "country": "Suecia",
        "currency": "Corona Sueca",
        "value": 11.15
    }
];

countryRange.addEventListener("input", function () {
    const selectedValue = parseInt(countryRange.value);
    selectedCountry.value = countryData[selectedValue].country;
    selectedCurrency.value = countryData[selectedValue].currency;
    currencyValue.value = "$" + countryData[selectedValue].value;
});