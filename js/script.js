const fileInput = document.getElementById("fileInput");
const previewImage = document.getElementById("previewImage");
const uploadButton = document.getElementById("uploadButton");

uploadButton.addEventListener("click", function () {
    fileInput.click();
});

fileInput.addEventListener("change", function () {
    const selectedFile = fileInput.files[0];

    if (selectedFile) {
        const reader = new FileReader();
        reader.onload = function (e) {
            previewImage.src = e.target.result;
            previewImage.style.display = "block";
        };
        reader.readAsDataURL(selectedFile);
    } else {
        previewImage.style.display = "none";
    }
});

const limpiarPreviewButton = document.getElementById("limpiarPreview");

limpiarPreviewButton.addEventListener("click", function () {
    previewImage.style.display = "none";
    fileInput.value = "";
});